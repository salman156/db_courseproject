using Microsoft.AspNetCore.Mvc;
using JiraTaskTracker.Models;
using JiraTaskTracker.Internal.Store;

namespace JiraTaskTracker.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CommentController : ControllerBase
{

    private readonly ILogger<IssueController> _logger;
    private readonly CommentStore _commentstore;


    public CommentController(ILogger<IssueController> logger, CommentStore userstore)
    {
        _logger = logger;
        _commentstore = userstore;
    }

    [HttpGet("{issue_ref:guid}")]
    public async Task<IEnumerable<Comment>> GetCommentsForIssue(Guid issue_ref)
    {
        return await _commentstore.GetComments(issue_ref);
    }

    [HttpPost]
    public async Task<Guid> CreateComment([FromBody] CreateCommentRequest create_request)
    {
        Comment comm = new Comment();
        comm.Author = create_request.Author; 
        comm.Issue = create_request.Issue;
        comm.Commentary = create_request.Commentary;
        comm.Created = DateTime.Now;

        return await _commentstore.CreateComment(comm);
    }

    [HttpPut]
    public async Task UpdateComment([FromBody] UpdateCommentRequest update_request)
    {   
        Comment comm = new Comment();
        comm.Commentary = update_request.Commentary;
        comm.Reference = update_request.Reference;
        await _commentstore.UpdateComment(comm);
    }


    [HttpDelete]
    public async Task DeleteComment([FromBody] Guid comment_ref)
    {   
        await _commentstore.DeleteComment(comment_ref);
    }
}
