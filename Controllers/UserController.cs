using Microsoft.AspNetCore.Mvc;
using JiraTaskTracker.Models;
using JiraTaskTracker.Internal.Store;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;

namespace JiraTaskTracker.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UserController : ControllerBase
{

    private readonly ILogger<UserController> _logger;
    private readonly UserStore _userstore;


    public UserController(ILogger<UserController> logger, UserStore userstore)
    {
        _logger = logger;
        _userstore = userstore;
    }

    [HttpGet]
    public async Task<IEnumerable<User>> GetUsers()
    {
        return await _userstore.GetUsers();
    }
    [Route("login")]
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public async Task<ActionResult> LoginUser([FromBody] UserLoginRequest login_request)
    {
        User user = await _userstore.GetUser(login_request.Name);

        string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: login_request.Password,
            salt: Convert.FromBase64String(user.Password_Salt),
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 100000,
            numBytesRequested: 256 / 8));
        
        if(user.Password_Hash == hashed)
            return Ok(); //TODO : Add JWE
        else
            return Unauthorized();
    }

    [HttpPost]
    public async Task<Guid> RegisterUser([FromBody] UserRegisterRequest register_request)
    {
        string password = register_request.Password;
        
        byte[] salt_bytes = new byte[128 / 8];
        using (var rngCsp = new RNGCryptoServiceProvider())
        {
            rngCsp.GetNonZeroBytes(salt_bytes);
        }
        string salt = Convert.ToBase64String(salt_bytes);

        // derive a 256-bit subkey (use HMACSHA256 with 100,000 iterations)
        string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: register_request.Password,
            salt: salt_bytes,
            prf: KeyDerivationPrf.HMACSHA256,
            iterationCount: 100000,
            numBytesRequested: 256 / 8));
        
        return await _userstore.CreateUser(register_request.Name, hashed, salt);
    }
}
