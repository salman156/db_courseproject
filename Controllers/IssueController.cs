using Microsoft.AspNetCore.Mvc;
using JiraTaskTracker.Models;
using JiraTaskTracker.Internal.Store;


namespace JiraTaskTracker.Controllers;

[ApiController]
[Route("api/[controller]")]
public class IssueController : ControllerBase
{

    private readonly ILogger<IssueController> _logger;
    private readonly IssueStore _issuestore;


    public IssueController(ILogger<IssueController> logger, IssueStore userstore)
    {
        _logger = logger;
        _issuestore = userstore;
    }

    [HttpGet]
    public async Task<IEnumerable<Issue>> GetIssues()
    {
        return await _issuestore.GetIssues();
    }

    [HttpPost]
    public async Task<Guid> CreateIssue([FromBody] CreateIssueRequest create_request)
    {
        Issue issue = new Issue();
        issue.Reporter = create_request.Reporter; 
        issue.Topic = create_request.Topic;
        issue.Priority = create_request.Priority;
        issue.Created = DateTime.Now;

        return await _issuestore.CreateIssue(issue);
    }


    [HttpPatch]
    public async Task ChangeAssignee([FromBody] ChangeAssigneeRequest change_request)
    {
        Issue issue = new Issue();
        issue.Reference = change_request.Reference; 
        issue.Assignee = change_request.Assignee;
        
        await _issuestore.UpdateAssignee(issue);
    }

    [Route("status")]
    [HttpPatch]
    public async Task ChangeIssueStatus([FromBody] ChangeIssueStatusRequest change_request)
    {
        Issue issue = new Issue();
        issue.Reference = change_request.Reference; 
        issue.Status = change_request.Status;
        
        await _issuestore.ChangeIssueStatus(issue);
    }
}
