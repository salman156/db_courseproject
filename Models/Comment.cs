namespace JiraTaskTracker.Models;

public class Comment {
    public Guid Reference {get; set;}
    
    public Guid Issue {get; set;}

    public Guid Author {get; set;}

    public string? Commentary {get; set;}

    public DateTime Created {get; set;}
}