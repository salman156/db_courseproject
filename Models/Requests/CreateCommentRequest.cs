using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace JiraTaskTracker.Models;

public class CreateCommentRequest {
    [Required]
    public Guid Issue {get; set;}

    [Required]
    public Guid Author {get; set;}

    [Required(AllowEmptyStrings = false)]
    public string? Commentary {get; set;}
}