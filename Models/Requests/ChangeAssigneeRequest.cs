using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace JiraTaskTracker.Models;

public class ChangeAssigneeRequest {
    [Required]
    public Guid Reference {get; set;}

    [Required]
    public Guid Assignee {get; set;}
}