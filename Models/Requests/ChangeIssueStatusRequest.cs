using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace JiraTaskTracker.Models;

public class ChangeIssueStatusRequest {
    [Required]
    public Guid Reference {get; set;}

    [Required]
    public IssueStatus Status {get; set;}
}