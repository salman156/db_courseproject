using System.ComponentModel.DataAnnotations;

namespace JiraTaskTracker.Models;

public class UserRegisterRequest {
    
    [Required(AllowEmptyStrings = false)]
    public string? Name {get; set;}

    [Required(AllowEmptyStrings = false)]
    public string? Password {get; set;}
}