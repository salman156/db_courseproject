using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace JiraTaskTracker.Models;

public class UpdateCommentRequest {
    [Required]
    public Guid Reference {get; set;}

    [Required]
    public string? Commentary {get; set;}
}