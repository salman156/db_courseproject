using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace JiraTaskTracker.Models;

public class CreateIssueRequest {
    [Required]
    public Guid Reporter {get; set;}

    [DefaultValue(0)]
    public int Priority {get; set;}

    [Required(AllowEmptyStrings = false)]
    public string? Topic {get; set;}
}