
namespace JiraTaskTracker.Models;

public enum IssueStatus : int
{
    InProgress = 0,
    OnPause = 1,
    Closed = 2,
}