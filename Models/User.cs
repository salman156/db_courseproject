namespace JiraTaskTracker.Models;

public class User {
    public Guid Reference {get; set;}

    public string? Name {get; set;}

    public string? Password_Hash {get; set;}

    public string? Password_Salt {get; set;}
}