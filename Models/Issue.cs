namespace JiraTaskTracker.Models;

public class Issue 
{
    public Guid Reference {get; set;}
    
    public Guid Reporter {get; set;}

    public Guid Assignee {get; set;}

    public Guid Project {get; set;}

    public int Priority {get; set;}

    public string? Topic {get; set;}

    public IssueStatus Status {get; set;}

    public string? Description {get; set;}

    public DateTime Created {get; set;}
}