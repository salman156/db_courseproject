using Npgsql;
using Dapper;
using JiraTaskTracker.Models;

namespace JiraTaskTracker.Internal.Store;


public class UserStore
{
    private static readonly string connectionString = "User ID=postgres;Password=admin15;Host=localhost;Port=5432;Database=db;";
    private NpgsqlConnection GetOpenConnection()
    {
        var conn = new NpgsqlConnection(connectionString);
        return conn;
    }

    public async Task<IEnumerable<User>> GetUsers()
    {
        using (var db = GetOpenConnection())
        {
            return await db.QueryAsync<User>(@"SELECT * FROM users");
        }
    }

    public async Task<User> GetUser(string name)
    {
        using (var db = GetOpenConnection())
        {
            return await db.QuerySingleAsync<User>(@"SELECT * FROM users WHERE name = @Name", new {Name = name});
        }
    }

    public async Task<Guid> CreateUser(string name, string password_hash, string password_salt)
    {
        using (var db = GetOpenConnection())
        {
            return  await db.QuerySingleAsync<Guid>(@"INSERT INTO users 
            (name, password_hash, password_salt)
            VALUES (@name, @hash, @salt) RETURNING reference", 
            new {name = name, hash = password_hash, salt = password_salt});
        }
    }

}