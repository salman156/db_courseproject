using Npgsql;
using Dapper;
using JiraTaskTracker.Models;

namespace JiraTaskTracker.Internal.Store;


public class CommentStore
{
    private static readonly string connectionString = "User ID=postgres;Password=admin15;Host=localhost;Port=5432;Database=db;";
    private NpgsqlConnection GetOpenConnection()
    {
        var conn = new NpgsqlConnection(connectionString);
        return conn;
    }

    public async Task<IEnumerable<Comment>> GetComments(Guid issue_ref)
    {
        using (var db = GetOpenConnection())
        {
            return await db.QueryAsync<Comment>(@"SELECT * FROM comments
            WHERE issue = @Issue",
            new {Issue = issue_ref});
        }
    }


    public async Task<Guid> CreateComment(Comment comm)
    {
        using (var db = GetOpenConnection())
        {
            return  await db.QuerySingleAsync<Guid>(@"INSERT INTO comments 
            (issue, author, commentary, created )
            VALUES (@Issue, @Author, @Commentary, @Created) RETURNING reference", 
            new {Issue = comm.Issue, Author = comm.Author,
            Commentary = comm.Commentary, Created = comm.Created});
        }
    }

    public async Task UpdateComment(Comment comm)
    {
        using (var db = GetOpenConnection())
        {
            await db.ExecuteAsync(@"UPDATE comments 
            SET commentary = @Commentary
            WHERE reference = @Reference", 
            new {Reference = comm.Reference, Commentary = comm.Commentary});
        }
    }
    public async Task DeleteComment(Guid comm_ref)
    {
        using (var db = GetOpenConnection())
        {
            await db.ExecuteAsync(@"DELETE FROM comments 
            WHERE reference = @Reference", 
            new {Reference = comm_ref});
        }
    }

}