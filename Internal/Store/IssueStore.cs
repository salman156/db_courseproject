using Npgsql;
using Dapper;
using JiraTaskTracker.Models;

namespace JiraTaskTracker.Internal.Store;


public class IssueStore
{
    private static readonly string connectionString = "User ID=postgres;Password=admin15;Host=localhost;Port=5432;Database=db;";
    private NpgsqlConnection GetOpenConnection()
    {
        var conn = new NpgsqlConnection(connectionString);
        return conn;
    }

    public async Task<IEnumerable<Issue>> GetIssues()
    {
        using (var db = GetOpenConnection())
        {
            return await db.QueryAsync<Issue>(@"SELECT * FROM issues");
        }
    }
    public async Task<Guid> CreateIssue(Issue issue)
    {
        using (var db = GetOpenConnection())
        {
            return  await db.QuerySingleAsync<Guid>(@"INSERT INTO issues 
            (reporter, priority, topic, created )
            VALUES (@Reporter, @Priority, @Topic, @Created) RETURNING reference", 
            new {Reporter = issue.Reporter, Priority = issue.Priority,
            Topic = issue.Topic, Created = issue.Created});
        }
    }

    public async Task UpdateAssignee(Issue issue)
    {
        using (var db = GetOpenConnection())
        {
            await db.ExecuteAsync(@"UPDATE issues 
            SET assignee = @Assignee
            WHERE reference = @Reference", 
            new {Assignee = issue.Assignee, Reference = issue.Reference});
        }
    }

    public async Task ChangeIssueStatus(Issue issue)
    {
        using (var db = GetOpenConnection())
        {
            await db.ExecuteAsync(@"UPDATE issues 
            SET status = @Status
            WHERE reference = @Reference", 
            new {Status = issue.Status, Reference = issue.Reference});
        }
    }

}